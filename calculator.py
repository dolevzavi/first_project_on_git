expression = input('enter expression: ')
[first_number, sign, second_number] = expression.split(' ')

if sign == '+':
    print(int(first_number) + int(second_number))
elif sign == '-':
    print(int(first_number) - int(second_number))
elif sign == '*':
    print(int(first_number) * int(second_number))
elif sign == '/':
    print(int(first_number) / int(second_number))